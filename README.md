<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Block Model Example - City</title>
  <style>
    #square{
      width: 600px;
      height: 600px;
      border: 10px solid #999;
      padding: 10px;
    }
    #park{
      width: 400px;
      height: 400px;
      background-color: lightgreen;
      float: left;
      margin-bottom: 10px;
      margin-right: 10px;
      
    }
    #building-1{
      width: 180px;
      height: 400px;
      background-color: lightblue;
      float:right;
      margin-left: 10px;
      margin-bottom: 10px;
    }
    #building-2{
      width: 400px;
      height: 180px;
      background-color: lightblue;
      float:left  ;
      margin-top: 10px;
    }
    #market{
      background-color: lightgrey;
      width: 180px;
      height: 180px;
      float:right;
      margin-left: 10px;
      margin-top: 10px;
    }
    #market .monument{
      font-size: 50px;
      padding: 65px;
      padding-bottom: 50px;
      
    }
    #park .tree{
      font-size: 50px;
      color: green;
      float: left;
      padding-left: 40px;
      padding-top: 50px;
      margin-right: 30px;
      padding-bottom: 50px;
      margin-top: 20px;
    }

  </style>
</head>
<body>

  <section id="square">

    <div id="park">
      <div class="tree">&#5848;</div>
      <div class="tree">&#5848;</div>
      <div class="tree">&#5848;</div>
      <div class="tree">&#5848;</div>
      <div class="tree">&#5848;</div>
      <div class="tree">&#5848;</div>
      <div class="tree">&#5848;</div>
      <div class="tree">&#5848;</div>
    </div>
    <div id="building-1"></div>
    <div id="building-2"></div>
    <div id="market">
      <div class="monument">
        &#9816;
      </div>
    </div>

  </section>


</body>
</html>
